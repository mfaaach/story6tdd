from django import forms

class StatusForm(forms.Form):
    status = forms.CharField(max_length=300, label= '', required=True)
    status.widget.attrs['class'] = 'form-control'
    status.widget.attrs['placeholder'] = 'Your Status'