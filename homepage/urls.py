from django.urls import path
from .views import index, delete_status

app_name = 'homepage'

urlpatterns = [
    path('', index, name='index'),
    path('<int:query_id>', delete_status, name='delete'),
]