from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.http import request
from importlib import import_module
from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve
from django.conf import settings
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

from .views import *
from .models import *

# Create your tests here.
class StatusUnitTest(TestCase):

    def test_status_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_status_url_doesnt_exist(self):
        response = Client().get('/wek')
        self.assertEqual(response.status_code, 404)
    
    def test_status_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_status_using_index_template2(self):
        response = Client().post('/', {'status':'OKAY'})
        self.assertTemplateUsed(response, 'index.html')

    def test_compare_input_status(self):
        statusNew = Status.objects.create(status='Coba Coba')
        status = Status.objects.all().count()
        self.assertEqual(status, 1)

    def test_str_is_equal_to_status(self):
        statusNew = Status.objects.create(status='Coba Coba')
        status = Status.objects.get(status='Coba Coba')
        self.assertEqual(str(status), status.status)

    def test_delete_status(self):
        obj = Status(status="asdasdasd")
        obj.save()

        response = self.client.get("/1")

        self.assertEqual(Status.objects.all().count(),0)

class StatusFunctionalTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser  = webdriver.Chrome('./chromedriver',chrome_options=chrome_options)
        self.browser.implicitly_wait(25)
        super(StatusFunctionalTest, self).setUp()


    def tearDown(self):
        self.browser.quit()
        super(StatusFunctionalTest, self).tearDown()

    def test_no_status(self):
        selenium = self.browser
        # Opening the link we want to test
        selenium.get(self.live_server_url)
        # find the form element
        
        assert '--- No Status Yet ---' in selenium.page_source

    def test_input_status(self):
        selenium = self.browser
        # Opening the link we want to test
        selenium.get(self.live_server_url)
        # find the form element
        status = selenium.find_element_by_id('id_status')

        submit = selenium.find_element_by_id('submit')

        # Fill the form with data
        time.sleep(2)
        status.send_keys('Coba Coba')

        # submitting the form
        time.sleep(2)
        submit.send_keys(Keys.RETURN)

        time.sleep(2)
        assert 'Coba Coba' in selenium.page_source

    def test_delete_status(self):
        selenium = self.browser
        # Opening the link we want to test
        selenium.get(self.live_server_url)
        # find the form element
        status = selenium.find_element_by_id('id_status')

        submit = selenium.find_element_by_id('submit')

        # Fill the form with data
        time.sleep(2)
        status.send_keys('Coba Coba')

        # submitting the form
        time.sleep(2)
        submit.send_keys(Keys.RETURN)

        time.sleep(2)
        delete = selenium.find_element_by_id('post-1')
        delete.send_keys(Keys.RETURN)

        time.sleep(5)
        assert 'Coba Coba' not in selenium.page_source